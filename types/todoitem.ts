export default interface TodoItem {
  name: string
  id: string
  done: boolean | undefined
}
