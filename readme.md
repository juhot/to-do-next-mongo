# To Do

An app made with Next.js and MongoDB.

## Database

Run an instance with e.g. Docker:

docker run --name mongodb -d -p 27017:27017 mongo

## Configure

Populate .env.local with something like:

`MONGODB_URI=mongodb://localhost:27017`

## Development

Run with

`npm install`

`npm run dev`

## Screenshot

![](screenshot.png)