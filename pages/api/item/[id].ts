import clientPromise from "../../../lib/mongodb"
import { ObjectId } from "mongodb"
import type { NextApiRequest, NextApiResponse } from "next"
import TodoItem from "../../../types/todoitem"

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id
  if (Array.isArray(id) || id === undefined) {
    return res.status(400).end()
  }
  switch (req.method) {
    case "PUT":
      const rawBody = req.body
      if (Array.isArray(rawBody) || rawBody === undefined) {
        return res.status(400).end()
      }
      const body = JSON.parse(rawBody)
      return await modifyStatus(id, body, res)
    case "DELETE":
      return await deleteItem(id, res)
    default:
      res.status(405).end()
  }
}

async function modifyStatus(id: string, body: TodoItem, res: NextApiResponse) {
  const filter = { _id: new ObjectId(id) }
  const change = { $set: { done: body.done } }
  const collection = await getCollection()
  const result = await collection.updateOne(filter, change)
  if (result.modifiedCount === 0) {
    res.status(422).end()
  }
  res.status(200).end()
}

async function deleteItem(id: string, res: NextApiResponse) {
  const doc = { _id: new ObjectId(id) }
  const collection = await getCollection()
  const result = await collection.deleteOne(doc)
  if (result.deletedCount === 0) {
    res.status(422).end()
  }
  res.status(200).end()
}

async function getCollection() {
  const client = await clientPromise
  const db = client.db("todo")
  return db.collection("items")
}
