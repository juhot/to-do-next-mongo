import clientPromise from "../../../lib/mongodb"
import type { NextApiRequest, NextApiResponse } from "next"

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const client = await clientPromise
  const db = client.db("todo")
  const body = JSON.parse(req.body)
  const doc = { name: body.content, done: false }
  await db.collection("items").insertOne(doc)
  res.status(200).end()
}
