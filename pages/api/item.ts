import clientPromise from "../../lib/mongodb"
import type { NextApiRequest, NextApiResponse } from "next"

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const client = await clientPromise
  const db = client.db("todo")
  const all = (await db.collection("items").find().toArray()).map((item) => {
    return { ...item, id: item._id }
  })
  res.status(200).json(all)
}
