import Head from "next/head"
import styles from "../styles/Home.module.css"
import { ChangeEvent, FormEvent, useEffect, useState } from "react"
import TodoItem from "../types/todoitem"

export default function Home() {
  const [data, setData] = useState<TodoItem[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [newContent, setNewContent] = useState("")
  const changeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    setNewContent(event.target.value)
  }

  const addItem = (event: FormEvent<HTMLFormElement>) => {
    setLoading(true)
    event.preventDefault()
    const body = JSON.stringify({ content: newContent })
    fetch("/api/item/create", { method: "POST", body }).then(() => {
      fetchItems()
    })
  }

  const changeStatus = (item: TodoItem, event: ChangeEvent<HTMLInputElement>) => {
    const body = JSON.stringify({ done: event.target.checked })
    fetch("/api/item/" + item.id, { method: "PUT", body: body }).then(() => {
      fetchItems()
    })
  }

  const removeItem = (item: TodoItem) => {
    setLoading(true)
    fetch("/api/item/" + item.id, { method: "DELETE" }).then(() => {
      fetchItems()
    })
  }

  const fetchItems = () => {
    fetch("/api/item")
      .then((res) => res.json())
      .then((data) => {
        setData(data)
        setLoading(false)
      })
  }

  useEffect(() => {
    setLoading(true)
    fetchItems()
  }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>To-do</title>
      </Head>

      <main className={styles.main}>
        <div className={styles.grid}>
          <h1 className={styles.title}>To-do</h1>
          {loading ? (
            <p>Loading...</p>
          ) : (
            <form className={styles.cardForm} onSubmit={addItem}>
              <input
                className={styles.cardInput}
                type="text"
                name="new-item"
                onChange={changeHandler}
                placeholder="Add new item"
              />
            </form>
          )}

          <div className={styles.list}>
            {data.map((item) => (
              <div className={styles.card} key={item.id}>
                <p>{item.name}</p>
                <input
                  className={styles.pushRight}
                  type="checkbox"
                  checked={item.done}
                  onChange={(event) => changeStatus(item, event)}
                ></input>
                <button
                  className={styles.remove}
                  onClick={() => removeItem(item)}
                >
                  <span className="material-icons md-18">delete_forever</span>
                </button>
              </div>
            ))}
          </div>
        </div>
      </main>
    </div>
  )
}
